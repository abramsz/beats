# beats

A metrics recorder via REST api service

# apis
|Action|Method||
|:-----|:-----|:-----|
|GET|/api/metrics| Get all of metrics|
|PUT|/api/metrics/:metricsId|Add a new metrics|
|PUT|/api/metrics/:metricsId/record|Add a new record for a metrics|
|GET|/api/metrics/:metricsId/record/:recordId|Get a record|
|DELETE|/api/metrics/:metricsId/record/:recordId|Remove a record|

