package main

import (
	"context"
	"flag"
	"gitea.com/abramsz/beats/cmd/beats/config"
	"gitea.com/abramsz/beats/cmd/beats/wire"
	"gitea.com/abramsz/beats/pkg/signal"
	_ "github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"golang.org/x/sync/errgroup"
)

func main() {
	var configFile string
	flag.StringVar(&configFile, "c", "config/application.yml", "Global Configuration File")
	flag.Parse()

	// Config initLogger
	conf, err := config.Load(configFile)
	if err != nil {
		log.Fatal().Err(err).Msg(err.Error())
	}

	// Logger initLogger
	initLog(conf)

	ctx := signal.WithContext(
		context.Background(),
	)

	app, err := wire.InitializeApplication(conf)
	if err != nil {
		log.Fatal().Err(err).Msg("main: cannot initialize server")
	}

	g := errgroup.Group{}

	// Start web server
	g.Go(func() error {
		log.Info().Msg("starting the http server")
		return app.Server.ListenAndServe(ctx)
	})

	if err := g.Wait(); err != nil {
		log.Fatal().Err(err).Msg("program terminated")
	}
}

func initLog(config config.Config) {
	logging := config.Logging

	if logging.Level == "debug" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

}
