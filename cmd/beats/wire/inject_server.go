package wire

import (
	"gitea.com/abramsz/beats/cmd/beats/config"
	"gitea.com/abramsz/beats/handler/api"
	"gitea.com/abramsz/beats/pkg/server"
	"github.com/google/wire"
	"github.com/gorilla/mux"
)

var serverSet = wire.NewSet(
	api.New,
	provideRouter,
	provideServer,
)

func provideRouter(api api.Server) *mux.Router {
	r := mux.NewRouter()
	api.Handler(r.PathPrefix("/api").Subrouter())
	return r
}

func provideServer(handler *mux.Router, config config.Config) *server.Server {
	return &server.Server{
		Addr:    config.Server.Address,
		Handler: handler,
	}
}
