package core

import "github.com/gorilla/mux"

type RouterRegister interface {
	Register(r *mux.Router)
}
