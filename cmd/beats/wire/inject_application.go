package wire

import "gitea.com/abramsz/beats/pkg/server"

type application struct {
	Server *server.Server
}

func newApplication(server *server.Server) application {
	return application{
		server,
	}
}
