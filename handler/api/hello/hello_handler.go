package hello

import (
	"gitea.com/abramsz/beats/core"
	"gitea.com/abramsz/beats/handler/api/render"
	"gitea.com/abramsz/beats/store/shared/db"
	"github.com/gorilla/mux"
	"net/http"
)

//------------- Resource ------------
type (
	Resource struct {
		Name string `json:"name"`
	}
)

//------------- countryHandler ------------
type helloHandler struct {
	*db.DB
}

func New(db *db.DB) core.HelloHandler {
	return &helloHandler{db}
}

func (h *helloHandler) Register(r *mux.Router) {
	r.HandleFunc("/hello", h.Get()).Methods(http.MethodGet)
}

func (h *helloHandler) Get() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		render.JsonOK(w, Resource{Name: "hello"})
	}
}
