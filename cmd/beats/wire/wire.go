//+build wireinject

package wire

import (
	"gitea.com/abramsz/beats/cmd/beats/config"
	"github.com/google/wire"
)

func InitializeApplication(config config.Config) (application, error) {
	wire.Build(
		handlerSet,
		storeSet,
		serverSet,
		newApplication,
	)

	return application{}, nil
}
