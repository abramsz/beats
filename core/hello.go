package core

import "net/http"

type HelloHandler interface {
	RouterRegister
	Get() http.HandlerFunc
}
