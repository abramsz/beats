package wire

import (
	"gitea.com/abramsz/beats/handler/api/hello"
	"github.com/google/wire"
)

var handlerSet = wire.NewSet(
	hello.New,
)
