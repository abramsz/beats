package server

import (
	"context"
	"golang.org/x/sync/errgroup"
	"net/http"
)

type Server struct {
	Addr    string
	Handler http.Handler
}

func (s *Server) ListenAndServe(ctx context.Context) error {
	return s.listenAndServe(ctx)
}

func (s *Server) listenAndServe(ctx context.Context) error {
	var g errgroup.Group
	s1 := &http.Server{
		Addr:    s.Addr,
		Handler: s.Handler,
	}
	g.Go(func() error {
		select {
		case <-ctx.Done():
			return s1.Shutdown(ctx)
		}
	})

	g.Go(func() error {
		return s1.ListenAndServe()
	})

	return g.Wait()
}
