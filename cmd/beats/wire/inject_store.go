package wire

import (
	"gitea.com/abramsz/beats/cmd/beats/config"
	"gitea.com/abramsz/beats/store/shared/db"
	"github.com/google/wire"
)

var storeSet = wire.NewSet(
	provideDatabase,
)

func provideDatabase(config config.Config) (*db.DB, error) {
	return db.Connect(
		config.DataSource.Driver,
		config.DataSource.Url,
	)
}
