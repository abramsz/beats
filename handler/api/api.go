package api

import (
	"gitea.com/abramsz/beats/core"
	"gitea.com/abramsz/beats/handler/api/render"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

type Server struct {
	helloHandler core.HelloHandler
}

func New(
	helloHandler core.HelloHandler,
) Server {
	return Server{
		helloHandler: helloHandler,
	}
}

func (s *Server) Handler(r *mux.Router) {
	r.Use(RecoverHandler, LoggingHandler, handlers.CORS())
	s.helloHandler.Register(r)
}

func RecoverHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if e := recover(); e != nil {
				err, ok := e.(error)
				log.Error().Err(err).Msgf("Trapped panic: (%T) -> %s", e)
				if ok {
					render.Error(w, err, err.Error(), 500)
					return
				}
				render.Error(w, err, err.Error(), 500)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func LoggingHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		log.Debug().Msgf("Started %s %s", r.Method, r.URL.Path)
		next.ServeHTTP(w, r)
		log.Debug().Msgf("Completed %s in %v", r.URL.Path, time.Since(start))
	})
}
